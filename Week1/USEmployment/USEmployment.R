# US employment 

CPS <- read.csv("CPSData.csv")
names(CPS)

table(CPS$Industry)

sort(table(CPS$State))

summary(CPS$Citizenship)

str(CPS$Citizenship)
table(CPS$Race, CPS$Hispanic)

summary(CPS)

is.na(CPS$Married)
table(CPS$Region, is.na(CPS$Married))
table(CPS$Age, is.na(CPS$Married))
table(CPS$Sex, is.na(CPS$Married))
table(CPS$Citizenship, is.na(CPS$Married))

names(CPS)
table(CPS$State,is.na(CPS$MetroAreaCode))

table(CPS$Region, is.na(CPS$MetroAreaCode))
10674/(20010+10674)
8084/(25093+8084)

tapply(is.na(CPS$MetroAreaCode),CPS$State,mean)

MetroAreaMap<- read.csv("MetroAreaCodes.csv")
CountryMap <- read.csv("CountryCodes.csv")

CPS = merge(CPS, MetroAreaMap, by.x="MetroAreaCode", by.y="Code", all.x=TRUE)
str(CPS)
summary(CPS)

table(CPS$MetroArea)

sort(tapply((CPS$Hispanic),CPS$MetroArea,mean))

sort(tapply(CPS$Race == "Asian",CPS$MetroArea,mean))

sort(tapply(CPS$Education == "No high school diploma", CPS$MetroArea, mean, na.rm=TRUE))

names(CPS)
names(CountryMap)
CPS <- merge(CPS,CountryMap,by.x='CountryOfBirthCode', by.y='Code',all.x=TRUE)
str(CPS)

sort(summary(CPS$Country))


table(CPS$MetroArea == "New York-Northern New Jersey-Long Island, NY-NJ-PA", CPS$Country != "United States")

match("India", CPS$Country)
sort(tapply(CPS$Country=="Somalia", CPS$MetroArea,mean, na.rm=TRUE))